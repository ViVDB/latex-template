#!/usr/bin/env zsh

# Choose outut folder

# uncomment this if you want to use a folder on dik instead of ram
# not recommended for SSD drives
# select location in "/tmp" "."
# do
# 	break
# done
location="/tmp"

# choose example to run

select file in "citing_and_links" "images" "chemistry" "math" "code"
do
	break
done

# create example folder or clean it

if [[ ! -a $location/example ]];
then
	mkdir $location/example
elif [[ -n *(#qN) ]];
then
	if [[ ! -a $location/example/$file.pdf ]] # the folder was created for another example
	then
		rm -r $location/example/*
	fi
fi

# run LaTex

latexmk -lualatex --output-directory=$location/example $file.tex

# show output

# okular $location/example/*.pdf

echo "files in $location/example/"

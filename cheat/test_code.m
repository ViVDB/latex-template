function polygone(population, ~)
%
% population est une matrice à 2 colonnes contenant la natalité et
% la mortalité
%
% éèàêç test accent
%
% the second argument was to make more imprecise polygons, just to see
% something when you interpolate


%sort vectors
for i=1:2
	population(:,i)=sort(population(:,i));
end


%%%%% This makes the polygone by interpolation and gives a pretty graph,
%%%%% we use the default function in the end, but it was a nice exercice

%find cumulative proportions :
%inc is the number of people with each increment

Nata=zeros(    ceil(   length(population(:,1))/inc   ) +1  ,    1);

Morta=Nata;

%add the first values :
Nata(1)=population(1,1);
Morta(1)=population(1,2);

for i=1:ceil(length(population(:,1))/inc)-1

	A=(population(i*inc,:)+population(i*inc+1,:))/2;
	%A=(population(i*inc,:));

	Nata(i+1)= A(1);
	Morta(i+1)=A(2);

end

%add the last one where you don't make a mean :
Nata(length(Nata))=population(length(population(:,1)),1);
Morta(length(Morta))=population(length(population(:,1)),2);
Ordonnee=0:100*inc/length(population(:,1)):99;
%taking in account things that aren't rounded, we need to add the 100 separately
Ordonnee=[Ordonnee,100];

plot(Nata,Ordonnee,'-o');hold on ; plot(Morta,Ordonnee,'-o');



%cdfplot(population(:,1));hold on ; plot(population(:,2));
cdfplot(population(:,1));hold on ; cdfplot(population(:,2));


%estimation Be
loader
BeCum=find(Be(1)==population(:,1));
BeCum=BeCum/100;
if not(isempty(BeCum))
	plot(Be(1),BeCum(1),'k*')
	BeCum=(find(Be(2)==population(:,2)))/100;
	plot(Be(2),BeCum(1),'k*')

	legend('Natalité','Mortalité','Belgique Natalité', 'Belgique Mortalité');


else
	legend('Natalité','Mortalité')

end
ylabel('Fréquence cumulée')
xlabel('Taux en "pour mille"')


end

# LaTex Template

**Disclaimer: I used "you" a lot because I'm imagining someone who would read this, but you don't _have_ to do this, it is my opinion, mostly, and the reader does what he wants. It's mostly a reminder to myself on how to use this repository, as much as or more than a guide for some lost soul who finds this anyways.**

## Table of contents

* [Intro](#intro-everything-needs-an-intro)
* [Short (long) note about LaTex](#short-long-note-about-latex)
* [Repository map](#repository-map)
* [Writing guidelines](#writing-guidelines)
* [About this repository](#about-this-repository)
* [Additional resources](#additional-resources)
* [Roadmap of what needs to be done in here](#roadmap)

## Intro -- everything needs an intro

This template was made for reports of classes at the University of Liege. In order to make sense of it all, since there is a bunch of files, I'll explain here what is the purpose of each of them, and give general guidelines on how I intend to fill it (the latter more for myself, so I have a consistent writing syntax, rather than "to make sense of it all", but anyways).

This repository is in constant evolution and I probably won't update the Readme for every single change I make, and this file will probably be a bit behind the actual repo most of the time, but it should still be useful.

I invite anyone (I doubt anyone else than me will ever read this, but who knows) who reached this by chance and is new to LaTex to read a little bit, I tried my best to include comprehensive examples and make sure no function is too obscure.

Note the licence, too, you can do what you want with this, just keep the copyright, basically \[this statement is not legally binding and only the content of the licence is\].

## Short (long) note about LaTex

So, just a thing about "LaTex". It's a program (in everyday language) that transforms a text file with a few specific markers into a nice and pretty pdf (as well as the language itself, and, in everyday use, it probably points towards a few other things as well, although nothing official, luckily because everyday use is a mess of confusion). Except it's much more than a single program, it's several programs that do a similar thing.

So, if you are new to all this, you most likely have a program like [TexMaker](https://www.xm1math.net/texmaker/) (I spiritually add the TM or R or C or whatever if needed here, I have no idea whether I should) installed. It is your front-end, basically the window that allows to modify the text document[^1]. To transform your text document into a pdf, TexMaker can use at least 4 different engines: pdfLaTex, MiKTex, LuaLaTex, and XeLaTex (possibly with capital letters at different places in the word, it's confusing). All those engines work slightly differently and reach a similar but sometimes noticeably different result. At the very least, pdfLaTex is older and has less support for sometimes simple stuff like fonts. Some packages will only work with some engines, although it's rare, so it's good to know you can choose the engine somewhere in the settings. As far as I know LuaLaTex _should_ be compatible with most everything.

[^1]: any `.tex` file is a text document, usually with utf-8 encoding, even notepad under Windows could edit it.

Another good thing to know is that you don't _need_ TexMaker to transform the `.tex` file into pdf, you just need the engine. Now, I won't go into details about what needs or not to be installed or how, it's confusing, especially on Windows -- for me at least -- and there are different things yet for bibliographies, but; it should be possible under all operating systems to open a command line and compile the text document -- which you edited with a text editor like [Atom](https://atom.io/) -- into a nice pdf. In particular, at least on Linux, there is a command `latexmk` which magically compiles the `.tex` file as many times as needed and including bibliographies, so all the references work nicely[^2]. Using a text editor and command lines is my usual approach and the [makefile](./makefile) allows me to easily launch `latexmk` with the options I need and the LuaLaTex engine; I just type `make` in the terminal and the magic happens. Now for some reason sometimes there are troubles with this and you can't do it (I haven't investigated yet but I know I have one document in which `latexmk` doesn't work and I need to type `pdflatex *.tex && pdflatex *.tex` because anything else either crashes or changes the font for some reason, it's probably because I took a random template online with a lot of things I haven't read in it, don't do that because afterwards you're stuck with it), but most of the time it's pretty handy. The moral of this paragraph, though, is that if you downloaded this and use TexMaker you can delete the makefile and the auxiliary folder.

[^2]: If you compile a single time a document with `\ref{}` inside it, you will get `[?]` or something similar in your pdf because the first run is used to define all the labels, and the second run is used to link the references to the labels.

Another thing that's good to know is that some packages like [minted](https://www.ctan.org/pkg/minted) or [svg](https://www.ctan.org/pkg/svg) use other programs on your computer to do things. So if those programs don't exist it won't work, that's the first thing, and in addition if you want to allow them to use the programs you either need to add `--shellescape` or `-shellescape` in command line (see the manual or the help for help) or, probably, to modify something in your editor configuration (if you use something like TexMaker).

## Repository map

| File or directory | Use 	|
|---				|---	|
| [.git](./.git)	            | See the `.gitignore` line.|
| [auxiliary](./auxiliary) 		| This is where the makefile will put all the output files. |
| [cheat](./cheat)				| That's where you can get examples to do a few interesting or recurrent things, if you forgot. There is a bunch of file and folders (or, at the time of writing, 2020-06-27, there _will be_) with interesting stuff, the names are mostly explicit. Using the example.sh script allows to compile the examples individually in the example folder if needed.|
| [cheat/testing](./cheat/testing)| If you need to quickly test something and don't want to compile the entire document, you can go there and test if it works with a simple example. Normally the same packages as in the root directory are included, but there is no `test.tex` file, everything is in [doc.tex](./cheat/testing/doc.tex). |
| [code](./code)				| If you have a bunch of code files to include in your document, you can put them there or add a link towards your project, it will make your repository much cleaner than if you copy 30 files in the root folder. Note that to use the files in code you need to give `./code/file.c` rather than `file.c` in your `.tex` document. I suppose it is also possible to define the default directory of the package as `./code`, but that's beside the point, you need to mention your code files are in a subdirectory of your `.tex` file. |
| [figure](./figure)			| That folder is pretty similar to code, except for figures. The same note on the path needing to be added is valid. If you want to add tables in `.csv` format, I personally think it's better to put them there if there is more than 2 or three, for clarity, or put them in a dedicated "table" folder or something. Same for any other kind of documents |
| [.gitignore](./.gitignore)	| That file is there to avoid a bunch of temporary files getting on the online repository, if you have no idea what it is, check online, and know it has nothing to do with LaTex but Git is cool. |
| [bib.bib](./bib.bib)			| This is the file where you put your bibliography, with a specific format. Check online, or in the examples, and if you use scientific papers you can use the " button on [Google Scholar](https://scholar.google.com/) (I add spiritually the TM or R or C or whatever if needed here), there is an option to get the BibTex entry already formatted, pretty useful if you ask me.|
| [doc.pdf](doc.pdf)			| Obviously, `doc.pdf` is your output pdf. Although the output pdf generated with make is within the [auxiliary](./auxiliary) folder, so this is just a link. |
| [doc.tex](doc.tex)			| That's where the formatting, syntax and everything is. When you create the document or when you have written it, you go in this file and you make sure all the font is what you want, whether the title page is nice, etc. No document-wide option should be anywhere else. The text should be written in text.tex.|
| [makefile](makefile)			| This is to compile the document, with make, simply and easily, if you don't use an IDE (TexMaker). Since the additional files created annoy me, it removes everything that isn't tracked in git, and in the root directory, so you should remove the relevant line if you don't use git or if your document is super long and you don't want to generate again all those files, because it takes time.|
| [README.md](README.md)		| You shouldn't be wondering what this file is at this point. |
| [text.tex](text.tex)			| I separated the text and the options, so when you write your text you can have a single document with only text and not 100 lines of options at first. Arguably, the bibliography, table of contents and title page could be in there, but I found it easier to put them in doc.tex. Anyways, it's easy to change. And with two separated documents, when you write, you don't worry about what font it has or whether the section will have a nice style, you just write and you take care of syntax all at once. With the assignment next to you so you get the font and the font size right. |


## Writing guidelines

* **About indentation**: After long consideration, and a bit of investigation on what was done, I decided to not use tabs, but spaces. Two spaces represent one indentation, because more than that and the image in the subsubsection is on the wrong side of the screen in the `.tex` file. In addition, in order to know in what point of the structure the text is, every separation, be it section, subsection or chapter should induce an indent from the next line and on until the next section, subsection, or whatever else. In the same idea, environments (`\begin{figure}` → `\end{figure}`, or `\begin{center}` → `\end{center}`) should have an indent. An exception is made for single declarations of an environment if the end is clearly visible, and for tags in the text. For example :

```latex
\begin{figure}
  \centering % no indent after this the end is clearly at figure
  \includegraphics{stuff.png}
  \caption{ A little bit of \textit{text} }
  % no need to have different lines for caption and its content, because
  % it's short, and for textit, because it's inline and it could potentially
  % add spaces, were there none, around the text
\end{figure}
```

* **About brackets**: If there are brackets, for multi-line things, the brackets should be on separate line than the content. Depending on the context, it may be clearer to put the opening bracket on the same line than the opening command, or sometimes not, so that's up to the writer. For example :

```latex
\parbox[pos][height][contentpos]{width}{
  The very long content is on a different line. Lorem ipsum dolor sit amet,
  consectetur adipiscing elit. Pellentesque sollicitudin massa vel enim
  porttitor tempor. Nunc lobortis quis arcu nec rutrum. Donec vehicula
  fermentum urna et elementum. Orci varius natoque penatibus et magnis dis
  parturient montes, nascetur ridiculus mus. Quisque ante tellus, convallis a
  lobortis eu, interdum placerat mauris. Aenean accumsan lacinia massa nec
  lobortis. Nulla in ipsum et tortor mollis eleifend sit amet vel sapien.
  Nam ante quam, pharetra sed mauris et, feugiat vehicula sem. Praesent et
  ipsum sem.
}
```

Or sometimes it's clearer with the bracket on a different line, to me, but it's about taste, like most of the time brackets in code:

```latex
\ifodd \value{num}
{
  The number is odd! Maybe you are odd. This random number generator defines
  your future, so you are definitely odd.
}
\else
{
  The number is not odd! You may be normal and boring, because your future is.
  Or maybe this random generator is not wormhole-linked to a neuron in the far
  future. Who knows.
}
```

or

```latex
\parbox[pos][height][contentpos]{width}
{
  The very long content is on a different line. Lorem ipsum dolor sit amet,
  consectetur adipiscing elit. Pellentesque sollicitudin massa vel enim
  porttitor tempor. Nunc lobortis quis arcu nec rutrum. Donec vehicula
  fermentum urna et elementum. Orci varius natoque penatibus et magnis dis
  parturient montes, nascetur ridiculus mus. Quisque ante tellus, convallis a
  lobortis eu, interdum placerat mauris. Aenean accumsan lacinia massa nec
  lobortis. Nulla in ipsum et tortor mollis eleifend sit amet vel sapien.
  Nam ante quam, pharetra sed mauris et, feugiat vehicula sem. Praesent et
  ipsum sem.
}
```
* **About colons**: It's not about LaTex conventions but about text conventions. It is good to know that, in English, according to "the internet" in general, you need to put no space before any punctuation with two parts: you write ":", ";", "?", "!", ... directly after the previous word, and you add a space afterwards. In French, however, on ajoute un espace de chaque côté de la marque de ponctuation : comme ceci !

* **About figures, arrays, and code files**: If more than 2 or 3 files of any kind is needed to compile the document, it's better to add a directory and shove them in there, so there aren't 85 files in your folder and you can find your `text.tex` file easily. Then, in your tex file, you need to give the relative path (e.g. `figure/uliege.pdf` instead of `uliege.pdf`)

* **About numbering**: If you don't want a section or something to be numbered, you can do something like: `\section*{}`. This is nice and everything, but the counter that counts section won't update either. So, if you have a `\ref{previousSection}` that is number 6, the `\ref{currentSection}` (pointing to `\section*{Current}`) will probably be 6, or an error, and `\ref{nextSection}` will be 7. If you want no section to be numbered, you should re-define the section with renewcommand and not include a counter with "section" in it (see doc.tex, search 'section' in a `\renewcommand` argument, and [en.wikibooks.org/wiki/LaTeX/Counters](https://en.wikibooks.org/wiki/LaTeX/Counters)). I'm not sure if it's possible to not number sections but have references to their number anyways.

As for numbering equations, not all equations should be numbered. I haven't checked yet how to do that (as of the time of writing, 2020-06-27), but only very important equations should be numbered. In mathematical developments, all lines should be numbered. If you have `stuff= other stuff =yet other stuff`, on different lines, it may be better to use a single number. If you have two versions of the same equation, with a _simple_ change in between, and they are obviously the same, either the same number should be used, or a reference to the initial equation and no number. If you have a very short document and no equation is cited, numbers are overkill. If you have a very long document, you may want to number the equations per chapter or per section rather than ending with equation \# 587.

* **About naming conventions**: When there are labels, a lot of them, it can become confusing. The best way, I thing, to keep it simple is to name everything using a coherent scheme. Since atom seems to propose labels without ":" in their name, I will stick with a simple:

| Kind | Name |
|---|---|
| Figures | fig-name-of-fig |
| Equations | eq-name-of-equation |
| Tables 	| tab-name-of-tab |
| Section   | sec-name-of-section|
| Footnotes | note-name-of-note |
| Bibliography reference | nameOfRef, nothing else is needed as it's not the same command. The camelCase is because I'm not sure how biblatex handles '-' in names, although it probably doesn't pose any problem, I'm used to camelCase for that specific kinf of references. |

* **About new lines**: In LaTex, new lines may be confusing. What am I saying. They _are_ confusing. To me. I _think_ `\par` or two new lines both end the paragraph, and that `\\` and `\newline` make a new line. It may seem like the same thing, but it is not: a new line doesn't apply the modified syntax for paragraphs, if you use one, like indent and the spacing. For this template for example there would be problems of consistency if both were used in the document. I'm not sure about which is which exactly, but to be safe, and to have a clear and spaced out document, I stick to using `\\` only when in small boxes of text, where things need to be aligned and `\vspace{2m}` or `\\[2cm]` is used for spacing, like in the title page or in headers. The rest of the time, I use two "returns" so the paragraphs are well spaced out in the `.tex` document.

* If you work with other people, the guidelines should be established in advance. In addition, some people prefer a line length of maximum 80 characters, so depending on who you work with you may want to enable wrapping in your editor or manually go to the next line while typing.

## About this repository

* **About git, the testing folder and the pdf files**: Git is made to track _text_ files. Thus, it is not as efficient for pdf files. So, in order to only add relevant meaningful versions of your pdf to the git repository, you can use `git update-index --assume-unchanged auxiliary/doc.pdf`, and it won't update. If the online version of the git changes it, you'll have to resolve it manually. When you want to track the pdf again, you just do `git update-index --no-assume-unchanged auxiliary/doc.pdf` ([nwdthemes.com/2019/05/09/git-ignore-changes-in-tracked-file/](https://nwdthemes.com/2019/05/09/git-ignore-changes-in-tracked-file/)). Similarly, you don't want to track the random tries you make in the testing folder, so you can use the same commands. Note that, if you clone this repo and delete the testing folder, since I'm not sure whether that parameter is uploaded on GitLab, you may need to run the second command on `cheat/testing/*`. Don't pay attention to this if you have no idea what "cloning the repo" means. Also note that for now the pdf is in the .gitignore file, so it won't update at all, which is the best solution since it can be re-created anyways.

* **About copying this repo**: When copying this repo, all the history will be copied as well. The best is to copy the repo, remove anything useless like the README or the LICENSE (though the license needs to be included if the thing is gonna be public, with potentially another licence file applying to the content or something), remove the .git repository, and then restart a repo with  `git init`. This way you start with a clean repo.

## Additional resources

* [detexify.kirelabs.org](https://detexify.kirelabs.org) : find how to write specific symbols in LaTex

* [mathpix.com](https://mathpix.com) : Transform equations in LaTex

* [www.mathcha.io/](https://www.mathcha.io/) : make latex drawings

* [www.overleaf.com/learn](https://www.overleaf.com/learn) : general help

* [en.wikibooks.org/wiki/LaTeX](https://en.wikibooks.org/wiki/LaTeX) : general help

* [www.ctan.org](https://www.ctan.org) : find the documentation for a specific package

## Roadmap

* Examples :
  * listings minted examples → code example (inline, alone, from file)
  * tables (array and multirow packages + tables online + hline + csv)
  * text quirks, symbols and formatting (textbackslash, textit,...) (does new paragraph after section matter?) (multi-column?)
    * way to add appendix (nanofabrication, bio mat)
	* add tricks to avoid overfull lines and stuff \linebreak[0] http://www.texfaq.org/FAQ-overfull https://www.overleaf.com/learn/latex/Line_breaks_and_blank_spaces
	* dates!!!!
  * math
    * https://ftp.snt.utwente.nl/pub/software/tex/macros/unicodetex/latex/unicode-math/unicode-math.pdf
    * https://tex.stackexchange.com/questions/139699/equivalent-of-mathup-for-upright-text-in-math-mode
    * https://tex.stackexchange.com/questions/432463/put-non-italic-characters-in-equation
	* \varepsilon https://iupac.org/wp-content/uploads/2019/05/IUPAC-GB3-2012-2ndPrinting-PDFsearchable.pdf and unicode-math
	* check "..."
	* Displaybreak for align environment (see amsmath documentation and ass1 bio mat) (and add how to not have breaks as well)
  * pictures
    * afterpage package
* Should add a French translation at some point (maybe, I mean, it's not like I have many classes in French)
* tables/figures/equations in a separate document so as to be able to find them easily? And have an 'equation list' pdf document at the end?
* self-ordering appendix?
* re-write equation when citing it?
* add which symbols are conventionally written how from the presentation last year
* links in a different color since there is no more frame
* tex→djvu? tex→pandoc?
* wrap text around tables, figures,...
* probably add a few things I learned from the report of assignment 1 in mat bio
* add prerequisite makefile because latexmk checks _after_ make...
* add language.json to the template
* check in doc.tex: %\usepackage{tocloft} and %\addtolength{\cftsecnumwidth}{28pt} 
* add appendix section naming scheme

* add bibliography examples to bib.bib for easy website reference
